// Contains all the endpoints for our application
// We separate the routes such that "index.js" only contains the information on the server

const express = require("express");
const router = express.Router();

const taskController = require("../controllers/taskController");

// Routes

// Route to get all the tasks
router.get("/", (req, res) =>{
	taskController.getAllTasks().then(resultFromController => res.send(
		resultFromController))
});

router.post("/", (req, res) => {
	console.log(req.body)
	taskController.deleteTask(req.body).then(resultFromController => res.send(
		resultFromController))
});

router.delete("/:id", (req, res) => {
	console.log(req.params)
	taskController.createTask(req.params.id).then(resultFromController => res.send(
		resultFromController))
});

router.put("/:id", (req, res) => {
	console.log(req.params.id)
	console.log(req.body)
	taskController.updateTask(req.params.id, req.body).then(resultFromController => 
		res.send(resultFromController))
});
// Use "module.exports" to export the route object to use index.js
module.exports = router;