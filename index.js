// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3000;
const taskRoute = require("./routes/taskRoute");

app.use(express.json());

mongoose.connect("mongodb+srv://angelobalbar:admin123@cluster0.q4we0.mongodb.net/B204-to-dos?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);


let db = mongoose.connection;
//If a connection error occured, output in the console
db.on("error", console.error.bind(console, "Connection Error"));

//If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database."));

app.use("/tasks", taskRoute);

// Server listening
app.listen(port, () => console.log(`Now listening to port ${port}`))
